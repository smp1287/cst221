/* SETH FERRERI CST 221 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define READ 1
#define WRITTEN 2

int ary[10];

int flag=READ;
int nextConsume = 0;
int nextProduce = 0;
int value = -1;

pthread_mutex_t lockit=PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t signalit=PTHREAD_COND_INITIALIZER;

void *produce(void *thread_number){
   int i;
   printf("I am the producer and my thread number is %ld \n", (long) thread_number);

   for (i=0; i<=5;i++)
   {
      pthread_mutex_lock(&lockit);
         while (flag !=READ){
            pthread_cond_wait(&signalit,&lockit);
         }
	if ((ary[nextProduce%10])==-1)
	{
	   ary[nextProduce%10]=i+1;
	   nextProduce++;
	}

	   flag=WRITTEN;
	   pthread_cond_broadcast(&signalit);
	   pthread_mutex_unlock(&lockit);

    }/* end for*/

    printf("producer exiting\n");
 
    return(NULL);
}//end of producer

void *consume(void *thread_number){
    int i;
    printf("I am the consumer and my thread number is %ld \n",
    (long)thread_number);

    for (i=0;i<=5;i++)
    {
        pthread_mutex_lock(&lockit);
        while (flag != WRITTEN)
        {
            pthread_cond_wait(&signalit,&lockit);
        }

	    while (ary[nextConsume%10]!=-1)
	    {
	    printf("[CONSUMER nc/mod: %d m: %d] the data is: %d", nextConsume,nextConsume%10,ary[nextConsume%10]);
	    ary[nextConsume%10]=-1;
	    nextConsume++;
	    }
 
    flag=READ;

    pthread_cond_broadcast(&signalit);
    pthread_mutex_unlock(&lockit);
    }/* end for*/

    printf("consumer exiting\n");
    return(NULL);

} // end consumer
/*
void errorOutput(char[100] name, int var){
    if (var<0){
        printf("%s (error) output: %d",name,var);
        exit(var);
    }
}
*/
int main(){
    pthread_t p1,p2,c1,c2;
    int  iret1, iret2, iret3, iret4;

    for (int i=0;i<10;i++){
      ary[i]=-1;
    }

    iret1 = pthread_create(&p1, NULL, produce, (void*) 0);
    //errorOutput("iret1", iret1);
    iret2 = pthread_create(&p2, NULL, produce, (void*) 1);
    //errorOutput("iret2", iret2);
    iret3 = pthread_create(&c1, NULL, consume, (void*) 2);
    //errorOutput("iret3", iret3);
    iret4 = pthread_create(&c2, NULL, consume, (void*) 3);
    //errorOutput("iret4", iret4);

   pthread_join(p1, NULL);
   pthread_join(p2, NULL);
   pthread_join(c1, NULL);
   pthread_join(c2, NULL);

return 0;
}
