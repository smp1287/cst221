/*
SETH FERRERI CST221

2 people are taking turns entering the cookie jar to pick a cookie, but the jar is only big
enough for one person at a time
*/

#include <stdio.h> 
#include <pthread.h> 
#include <semaphore.h> 
#include <unistd.h> 
  
sem_t mutex; 
  
void* thread(void* arg) 
{ 
    //wait 
    sem_wait(&mutex); 
    printf("\nEntering cooking jar..\n"); 
  
    //critical section 
   //Figure out which cookie to choose
    sleep(4); 
      
    //signal 
    printf("\nExiting cookie jar...\n"); 
    sem_post(&mutex); 
} 
  
  
int main() 
{ 
    sem_init(&mutex, 0, 1); 
    pthread_t t1,t2; 
    pthread_create(&t1,NULL,thread,NULL); 
    sleep(2); 
    pthread_create(&t2,NULL,thread,NULL); 
    pthread_join(t1,NULL); 
    pthread_join(t2,NULL); 
    sem_destroy(&mutex); 
    return 0; 
} 